//
// Created by tim on 7/14/18.
//
#include "functions.h"
unsigned int factorial(unsigned int number) {
    return number <= 1 ? number : factorial(number - 1)*number;
}

unsigned int add(unsigned int a, unsigned int b) {
    return a + b;
}
