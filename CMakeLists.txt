cmake_minimum_required(VERSION 3.7)
project(my-cpp-project-template)
set(CMAKE_CXX_STANDARD 11)

# Build Options
option( BUILD_SHARED   "Build shared library."              OFF )
option( BUILD_EXAMPLES "Build examples applications."       OFF )
option( BUILD_TESTS    "Build all available test suites."   ON )

# Project includes (absolute paths prefered)
get_filename_component(INCLUDE "include" REALPATH BASE_DIR "${CMAKE_CURRENT_SOURCE_DIR}")
include_directories(${INCLUDE})

# Source files
file(GLOB SOURCE_FILES "src/functions.cpp")

# set path where to find cmake modules:
set(CMAKE_MODULE_PATH ${PROJECT_SOURCE_DIR}/cmake/modules)
# set path where to put the compiled executable binary:
set(EXECUTABLE_OUTPUT_PATH ${PROJECT_BINARY_DIR}/bin)
# set path where to put the compiled library binary:
set(LIBRARY_OUTPUT_PATH ${PROJECT_BINARY_DIR}/lib)

if(BUILD_TESTS)
    add_subdirectory("${PROJECT_SOURCE_DIR}/test/unit")
    #add_subdirectory("${PROJECT_SOURCE_DIR}/test/acceptance")
    #add_subdirectory("${PROJECT_SOURCE_DIR}/test/regression")
    #add_subdirectory("${PROJECT_SOURCE_DIR}/test/integration")
endif()

# create executable from source files
add_executable(main ${SOURCE_FILES} src/main.cpp)

# link exe with the libraries
# target_link_libraries(${EXE_NAME} ${FRUIT_LIBRARIES} ${B64_LIBRARIES})