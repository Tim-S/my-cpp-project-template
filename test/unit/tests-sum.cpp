//
// Created by tim on 7/14/18.
//

#include <catch.hpp>
#include "../../include/functions.h"

TEST_CASE( "Sums are computed", "[sum]" ) {
    REQUIRE(add(1, 1) == 2);
}

TEST_CASE( "Factorials are computed", "[factorial]" ) {
    REQUIRE( factorial(1) == 1 );
    REQUIRE( factorial(2) == 2 );
    REQUIRE( factorial(3) == 6 );
    REQUIRE( factorial(4) == 24 );
    REQUIRE( factorial(10) == 3628800 );
}