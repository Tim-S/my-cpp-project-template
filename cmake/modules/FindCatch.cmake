# Locate Catch include path
# Catch can be found at https://github.com/catchorg/Catch2

# This module defines
# CATCH_INCLUDE_DIR, where to find header file.
# CATCH_FOUND, If false, don't try to use Catch.

FIND_PATH(CATCH_INCLUDE_DIR catch.hpp
        PATHS
        "$ENV{CATCH}/include"
        "${PROJECT_SOURCE_DIR}/lib/catch/include"
        /usr/local/include
        /usr/include
        )

SET(CATCH_FOUND FALSE)
IF(CATCH_INCLUDE_DIR)
        MESSAGE(STATUS "Found Catch")
        SET(CATCH_FOUND TRUE)
ENDIF(CATCH_INCLUDE_DIR)

MARK_AS_ADVANCED(
        CATCH_INCLUDE_DIR
)